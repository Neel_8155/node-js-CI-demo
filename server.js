const Hapi = require('hapi');

const port = process.env.PORT || 3000; 
var server_host = process.env.YOUR_HOST || '0.0.0.0';

const server = Hapi.Server({
    host: server_host,
    port: port
});

const init = async () => {

    await server.start();
    console.log("Server up and running at port: " + port);

}

//Setup the routes
require('./routes/routes')(server);

init();
