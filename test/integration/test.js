'use strict';

var assertChai  = require('chai');
var request = require('request');

describe('API Tests', function() {
    it('Add Service Test', function(done) {
        request('http://127.0.0.1:3000/calculator/add/5/10' , function(error, response, body) {
            assertChai.expect(body).to.contains('15');
            done();
        });
    });
});

describe('API Tests', function() {
    it('Subsstraction Service Test', function(done) {
        request('http://127.0.0.1:3000/calculator/sub/5/10' , function(error, response, body) {
            assertChai.expect(body).to.contains('-5');
            done();
        });
    });
});
